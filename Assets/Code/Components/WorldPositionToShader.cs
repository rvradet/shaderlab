﻿using UnityEngine;

public class WorldPositionToShader : MonoBehaviour
{	
	void Update ()
    {
        // Set the property named "_Position" in our material's shader with current transform position value
        Shader.SetGlobalVector("_Position", transform.position);
	}
}
